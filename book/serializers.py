from rest_framework import serializers


from book.models import BookInfo, PeopleInfo


class PeopleInfoModelSerializer(serializers.ModelSerializer):
    book_id = serializers.IntegerField(required=False)

    class Meta:
        model = PeopleInfo
        fields = ['id', 'book_id', 'name', 'password', 'is_delete']
        extra_kwargs = {
            'password': {'write_only': True},
            'is_delete': {'read_only': True}
        }


class BookInfoModelSerializer(serializers.ModelSerializer):
    people = PeopleInfoModelSerializer(many=True, required=False)

    class Meta:
        model = BookInfo
        fields = '__all__'

    # def create(self, validated_data):
    #     print("validated_data", validated_data)
    #
    #     # 把 people 从 validated_data 里删除 并且返回
    #     peoples = validated_data.pop('people')
    #
    #     # 保存书
    #     book = BookInfo.objects.create(**validated_data)
    #
    #     # 保存人物
    #     for p in peoples:
    #         PeopleInfo.objects.create(book=book, **p)
    #
    #     return book


