import traceback

from django.views.generic import View
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from book.models import BookInfo, PeopleInfo
from django.http import JsonResponse, HttpResponse
import json

# Create your views here.
from book.serializers import BookInfoModelSerializer, PeopleInfoModelSerializer


# class BookListView(ListModelMixin, CreateModelMixin, GenericAPIView):
#
#     def get(self, request):
#         print(type(request))
#         print(request)
#         print(request.GET)
#         print(request.query_params)
#
#         # return Response(data={'name': 'xiaoming'})
#         return Response(data={'name': 'xiaoming'}, status=status.HTTP_200_OK)
#
#     def post(self, request):
#         print(request.data)
#         print(type(request.data))
#         return JsonResponse({})

# class BookListView(View):
#     """
#     查询所有图书、增加图书
#     """
#     def get(self, request):
#         """
#         查询所有图书
#         路由：GET /books/
#         """
#         queryset = BookInfo.objects.all()
#         # book_list = []
#         # for book in queryset:
#         #     book_list.append({
#         #         'id': book.id,
#         #         'name': book.name,
#         #         'pub_date': book.pub_date
#         #     })
#
#         bs = BookInfoModelSerializer(queryset, many=True)
#
#         return JsonResponse(bs.data, safe=False)
#
#     def post(self, request):
#         """
#         新增图书
#         路由：POST /books/
#         """
#
#         # book_dict = json.loads(request.body)
#         # name = book_dict.get('name'),
#         # pub_date = book_dict.get('pub_date')
#
#         # 此处详细的校验参数省略
#
#         # book = BookInfo.objects.create(
#         #     name=book_dict.get('name'),
#         #     pub_date=book_dict.get('pub_date')
#         # )
#         book_dict = json.loads(request.body)
#         name = book_dict.get('name')
#         pub_date = book_dict.get('pub_date')
#
#         book7 = BookInfo.objects.get(id=7)
#
#         # 1 字典数据
#         data_dict = {
#             "name": "哈龙钻钻钻",
#             "pub_date": "2022-03-17",
#             "readcount": 300,
#             "commentcount": 200
#         }
#         # 2 创建序列化器对象  把字典传进来
#         bs = BookInfoModelSerializer(data=data_dict)
#         # 验证
#         # print(bs.is_valid())
#         # print(bs.errors)
#         try:
#             bs.is_valid(raise_exception=True)
#             # 　保存数据到数据库
#             bs.save()
#         except Exception as e:
#             traceback.print_exc()  # 打印原始异常数据
#             print(e)
#             return JsonResponse({'code': 505})
#
#         return JsonResponse({})


# class BookDetailView(View):
#     """
#     获取单个图书信息
#     修改图书信息
#     删除图书
#     """
#     def get(self, request, pk):
#         """
#         获取单个图书信息
#         路由： GET  /books/<pk>/
#         """
#         try:
#
#             book = BookInfo.objects.get(id=pk)
#
#             # 创建序列化器对象  传入需要序列化的图书对象
#             bs = BookInfoModelSerializer(book)
#
#         except BookInfo.DoesNotExist:
#             return JsonResponse({},status=404)
#
#         return JsonResponse(bs.data)
#
#     def put(self, request, pk):
#         """
#         修改图书信息
#         路由： PUT  /books/<pk>
#         """
#         try:
#             book = BookInfo.objects.get(id=pk)
#         except BookInfo.DoesNotExist:
#             return JsonResponse({},status=404)
#
#         book_dict = json.loads(request.body)
#
#         # 此处详细的校验参数省略
#
#         book.name = book_dict.get('name')
#         book.pub_date = book_dict.get('pub_date')
#         book.save()
#
#         return JsonResponse({
#             'id': book.id,
#             'name': book.name,
#             'pub_date': book.pub_date
#         })
#
#     def delete(self, request, pk):
#         """
#         删除图书
#         路由： DELETE /books/<pk>/
#         """
#         try:
#             book = BookInfo.objects.get(id=pk)
#         except BookInfo.DoesNotExist:
#             return JsonResponse({},status=404)
#
#         book.delete()
#         print("删除")
#
#         return JsonResponse({},status=204)


# class PeopleDetailView(View):
#
#     def get(self, request, pk):
#         """
#         获取单个人物
#         """
#         try:
#             p = PeopleInfo.objects.get(id=pk)
#             ps = PeopleInfoModelSerializer(p)
#         except BookInfo.DoesNotExist:
#             return JsonResponse({},status=404)
#
#         return JsonResponse(ps.data)
#
#     def post(self, request, ):
#         # data_list = [
#         #     {
#         #         'book_id': 1,
#         #         'name': '靖妹妹',
#         #         'password': '123456abc',
#         #         'is_delete': 1
#         #     },
#         #     {
#         #         'book_id': 1,
#         #         'name': '靖表哥',
#         #         'password': '123456abc',
#         #         'is_delete': 1
#         #     },
#         #     {
#         #         'book_id': 1,
#         #         'name': '靖表弟',
#         #         'password': '123456abc',
#         #         'is_delete': 1
#         #     }
#         # ]
#         data = {
#             'name': '哈龙散文集',
#             'pub_date': '2022-06-24',
#             'people': [
#                 {
#                     'name': '靖妹妹111',
#                     'password': '123456abc'
#                 },
#                 {
#                     'name': '靖表哥222',
#                     'password': '123456abc'
#                 },
#                 {
#                     'name': '靖表哥333',
#                     'password': '123456abc'
#                 }
#             ]
#         }
#         # pms = PeopleInfoModelSerializer(data=data_list, many=True)
#         bms = BookInfoModelSerializer(data=data)
#         is_ok = bms.is_valid()
#         print(is_ok)
#         print(bms.errors)
#
#         bms.save()
#
#         return JsonResponse({})


class BookView(ModelViewSet):

    queryset = BookInfo.objects.all()
    serializer_class = BookInfoModelSerializer



