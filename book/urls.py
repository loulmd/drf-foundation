# from django.urls import path
# from book.views import BookListView
#
# # BookDetailView   PeopleDetailView
#
# urlpatterns = [
#     path('books/', BookListView.as_view()),
#     #path('books/<pk>', BookDetailView.as_view()),
#     #path('peoples/', PeopleDetailView.as_view()),
# ]


from rest_framework.routers import DefaultRouter

from book.views import BookView

urlpatterns = [

]

router = DefaultRouter()
router.register(prefix=r'books', viewset=BookView, basename='')

urlpatterns += router.urls